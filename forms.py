from flask_wtf import FlaskForm
from wtforms import (StringField, PasswordField, TextAreaField, IntegerField,
                    BooleanField, DateField, FloatField)
from wtforms.validators import (DataRequired, ValidationError, Regexp, Length,
                                EqualTo)

from models import (Docente, Alumno, Practica)

def doc_id_exists(form, field):
    if Docente.select().where(Docente.idDocente == field.data).exists():
        raise ValidationError('Ya existe un docente con ese id')


def alu_id_exists(form, field):
    if Alumno.select().where(Alumno.idAlumno == field.data).exists():
        raise ValidationError('Ya existe un alumno con esa boleta')

class RegisterDocForm(FlaskForm):
    idDocente = IntegerField(
        'ID Docente',
        validators=[
            DataRequired(),
            doc_id_exists
        ])
    Nombres = StringField(
        'Nombres',
        validators=[
            DataRequired()
        ])
    Apellido1 = StringField(
        'Apellido Paterno',
        validators=[
            DataRequired(),
        ])
    Apellido2 = StringField(
        'Apellido Materno',
        validators=[
            DataRequired()
        ])
    Genero = StringField(
        'Genero',
        validators=[
            DataRequired()
        ])
    password = PasswordField(
        'Password',
        validators=[
            DataRequired(),
            Length(min=4),
            EqualTo('password2', message='Los passwords deben coincidir')
        ])
    password2 = PasswordField(
        'Confirm Password',
        validators=[DataRequired()]
    )


class RegisterAluForm(FlaskForm):
    idAlumno = IntegerField(
        'Numero de Boleta',
        validators=[
            DataRequired(),
            alu_id_exists
        ])
    Nombres = StringField(
        'Nombres',
        validators=[
            DataRequired()
        ])
    Apellido1 = StringField(
        'Apellido Paterno',
        validators=[
            DataRequired()
        ])
    Apellido2 = StringField(
        'Apellido Materno',
        validators=[
            DataRequired()
        ])
    Telefono = IntegerField(
        'Telefono',
        validators=[
            DataRequired()
        ])
    NSS = StringField(
        'NSS',
        validators=[
            DataRequired()
        ])
    Genero = StringField(
        'Genero',
        validators=[DataRequired()])
    idGrupo = IntegerField(
        'idGrupo',
        validators=[
            DataRequired()
        ])


class RegisterPracForm(FlaskForm):
    NumeroPractica = IntegerField(
        'Numero de Practica',
        validators=[
            DataRequired()
        ])
    TipoDePractica = StringField(
        'Tipo de Practica',
        validators=[
            DataRequired()
        ])
    FechaRealizacion = DateField(
        'Fecha de Realizacion',
        validators=[
            DataRequired()
        ])
    PresupuestoAsignado = FloatField(
        'PresupuestoAsignado',
        validators=[
            DataRequired()
        ])
    Objetivo = TextAreaField(
        'Objetivo',
        validators=[
            DataRequired()
        ])
    CompetenciaADesarrollar = TextAreaField(
        'CompetenciaADesarrollar',
        validators=[
            DataRequired()
        ])
    EstrategiaDesarrollo = TextAreaField(
        'Estrategia de Desarrollo',
        validators=[
            DataRequired()
        ])
    idGrupo = IntegerField(
        'ID Grupo',
        validators=[
            DataRequired()
        ])
    idEmpresa = IntegerField(
        'ID Empresa',
        validators=[
            DataRequired()
        ])

class LoginForm(FlaskForm):
    idDocente = IntegerField('ID', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
