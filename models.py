import datetime

from flask_login import UserMixin
from flask_bcrypt import generate_password_hash

from peewee import *

DATABASE = SqliteDatabase('base.db')

class Grupo(Model):
    idGrupo = IntegerField(unique=True,null=False)
    NumeroAlumnos = IntegerField(null=True)
    NumeroDocentes = IntegerField(null=False)

    class Meta:
        database = DATABASE
        order_by = ('-idGrupo',)


class Alumno(Model):
    idAlumno = IntegerField(unique=True,null=False)
    Nombres = CharField(null=False)
    Apellido1 = CharField(null=False)
    Apellido2 = CharField(null=False)
    Telefono = IntegerField(null=True)
    NSS = CharField(unique=True,null=False)
    Genero = CharField(max_length=1,null=False)
    idGrupo = ForeignKeyField(
        Grupo,
        related_name='AlumnosGrupo',
    )

    class Meta:
        database = DATABASE
        order_by = ('-idAlumno',)

    @classmethod
    def create_alumno(cls, idAlumno, Nombres, Apellido1, Apellido2, Telefono,
                        NSS, Genero, idGrupo):
        try:
            with DATABASE.transaction():
                cls.create(
                    idAlumno=idAlumno,
                    Nombres = Nombres,
                    Apellido1 = Apellido1,
                    Apellido2 = Apellido2,
                    Telefono = Telefono,
                    NSS = NSS,
                    Genero = Genero,
                    idGrupo = idGrupo,
                )
        except IntegrityError:
            raise ValueError('Alumno ya existente')


class Empresa(Model):
    idEmpresa = AutoField(unique=True)
    Nombre = CharField(null=False)
    Pais = CharField(null=False)
    EntidadFederativa = CharField(null=False)

    class Meta:
        database = DATABASE
        order_by = ('-idEmpresa',)


class Practica(Model):
    idPractica = AutoField(primary_key=True,unique=True,null=False)
    NumeroPractica = IntegerField(unique=True,null=False)
    TipoDePractica = CharField(max_length=1,null=False)
    FechaRealizacion = DateField(null=False)
    PresupuestoAsignado = FloatField(null=False)
    Objetivo = TextField(null=False)
    CompetenciaADesarrollar = TextField(null=False)
    EstrategiaDesarrollo = TextField(null=False)
    PresupuestoEjercido = FloatField(null=True)
    idGrupo = ForeignKeyField(
        Grupo,
        related_name='PracticaGrupo',
    )
    idEmpresa = ForeignKeyField(
        Empresa,
        related_name='PracticaEmpresa',)

    class Meta:
        database = DATABASE
        order_by = ('-idPractica',)

    @classmethod
    def create_prac(cls, NumeroPractica, TipoDePractica, FechaRealizacion,
                    PresupuestoAsignado, Objetivo, CompetenciaADesarrollar,
                    EstrategiaDesarrollo, idGrupo, idEmpresa):
        try:
            with DATABASE.transaction():
                cls.create(
                    NumeroPractica=NumeroPractica,
                    TipoDePractica = TipoDePractica,
                    FechaRealizacion = FechaRealizacion,
                    PresupuestoAsignado = PresupuestoAsignado,
                    Objetivo = Objetivo,
                    CompetenciaADesarrollar = CompetenciaADesarrollar,
                    EstrategiaDesarrollo = EstrategiaDesarrollo,
                    PresupestoEjercido = PresupuestoAsignado,
                    idGrupo = idGrupo,
                    idEmpresa = idEmpresa,
                )
        except IntegrityError:
            raise ValueError('Practica ya existente')


class Formatos(Model):
    idFormatos = AutoField(unique=True,null=False)
    TipoFormato = IntegerField(null=False)
    idPractica = ForeignKeyField(
        Practica,
        related_name='FormatosPractica',)

    class Meta:
        database = DATABASE
        order_by = ('-idFormatos',)


class Historico(Model):
    idHistorico = AutoField(unique=True,null=False)
    FechaInicio = DateField(null=False)
    FechaFin = DateField(null=True)
    PracticaAprobada = BooleanField(null=False)
    idFormatos = ForeignKeyField(
        Formatos,
        related_name='HistoricoFormatos',)

    class Meta:
        database = DATABASE
        order_by = ('-idHistorico',)


class Administrador(UserMixin, Model):
    idAdministrador = IntegerField(unique=True,null=False)
    Nombres = CharField(null=False)
    Apellido1 = CharField(null=False)
    Apellido2 = CharField(null=False)
    password = CharField(null=False)

    class Meta:
        database = DATABASE
        order_by = ('-idAdministrador',)

    @classmethod
    def create_admin(cls, idAdministrador, Nombres, Apellido1, Apellido2,
                    password):
        try:
            with DATABASE.transaction():
                cls.create(
                    idAdministrador = idAdministrador,
                    Nombres = Nombres,
                    Apellido1 = Apellido1,
                    Apellido2 = Apellido2,
                    password = generate_password_hash(password),
                )
        except IntegrityError:
            raise ValueError('User already exists')


class Administra(Model):
    idAdministrador = ForeignKeyField(Administrador, related_name='AdministraA')
    idPractica = ForeignKeyField(Practica, related_name='AdministraP')

    class Meta:
        database = DATABASE
        indexes = (
            (('idAdministrador', 'idPractica'), True),
        )


class Docente(UserMixin,Model):
    idDocente = IntegerField(unique=True,null=False)
    Nombres = CharField(null=False)
    Apellido1 = CharField(null=False)
    Apellido2 = CharField(null=False)
    Genero = CharField(max_length=1,null=False)
    password = CharField(null=False)

    class Meta:
        database = DATABASE
        order_by = ('-idDocente',)

    @classmethod
    def create_doc(cls, idDocente, Nombres, Apellido1, Apellido2, Genero,
                    password):
        try:
            with DATABASE.transaction():
                cls.create(
                    idDocente=idDocente,
                    Nombres = Nombres,
                    Apellido1 = Apellido1,
                    Apellido2 = Apellido2,
                    Genero = Genero,
                    password = generate_password_hash(password),
                )
        except IntegrityError:
            raise ValueError('El docente ya existe')


class Asigna(Model):
    idGrupo = ForeignKeyField(Grupo, related_name='AsignaG')
    idDocente = ForeignKeyField(Docente, related_name='AsignaD')

    class Meta:
        database = DATABASE
        indexes = (
            (('idGrupo','idDocente'), True),)


class UnidadAcademica(Model):
    idUnidadAcademica = IntegerField(unique=True,null=False)
    Nombre = CharField(null=False)

    class Meta:
        database = DATABASE
        order_by = ('-idUnidadAcademica',)


class Carrera(Model):
    idCarrera = IntegerField(unique=True,null=False)
    Nombre = CharField(null=False)

    class Meta:
        database = DATABASE
        order_by = ('-idCarrera',)


class Asignatura(Model):
    idAsignatura = IntegerField(unique=True,null=False)
    Semestre = IntegerField(null=False)
    Periodo = CharField(null=False)
    idCarrera = ForeignKeyField(
        Carrera,
        related_name='AsignaturaCarrera',)

    class Meta:
        database = DATABASE
        order_by = ('-idAsignatura',)


class Posee(Model):
    idUnidadAcademica = ForeignKeyField(UnidadAcademica, related_name='PoseeUA')
    idCarrera = ForeignKeyField(Carrera, related_name='PoseeC')

    class Meta:
        database = DATABASE
        indexes = (
            (('idUnidadAcademica', 'idCarrera'), True),
        )


class Tiene(Model):
    idAsignatura = ForeignKeyField(Asignatura, related_name='TieneA')
    idGrupo = ForeignKeyField(Grupo, related_name='TieneG')

    class Meta:
        database = DATABASE
        indexes = (
            (('idAsignatura', 'idGrupo'), True),
        )


def initialize():
    DATABASE.connect()
    DATABASE.create_tables([Grupo, Alumno, Empresa, Practica, Formatos,
                            Historico, Administrador, Administra, Docente,
                            Asigna, UnidadAcademica, Carrera, Asignatura,
                            Posee, Tiene], safe=True)
    DATABASE.close()
