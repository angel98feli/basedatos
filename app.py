from flask import (Flask, g, render_template, flash, url_for, redirect, abort)

from flask_bcrypt import check_password_hash
from flask_login import (LoginManager, login_user, login_required, current_user,
                        logout_user, AnonymousUserMixin)

import models
import forms

DEBUG = True
PORT = 8000
HOST = '0.0.0.0'

app = Flask(__name__)
app.secret_key = 'ASFDXCYE!lfasdasd'';nljnAFON,.ASDOJJNLwuhebjlks23451'

class Anonyimous(AnonymousUserMixin):
    def __init__(self):
        self.username = 'Invitado'

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
login_manager.anonymous_user = Anonyimous


@login_manager.user_loader
def load_user(userid):
    try:
        return models.Docente.get(models.Docente.idDocente == userid)
    except models.DoesNotExist:
        return None


@app.before_request
def before_request():
    """Conecta a la base de Datos antes de cada request"""
    g.db = models.DATABASE
    if g.db.is_closed():
        g.db.connect()
        g.user = current_user


@app.after_request
def after_request(response):
    """Cerramos la conexión a la BD"""
    g.db.close()
    return response


@app.route('/register_alu', methods=('GET','POST'))
def register_alu():
    form = forms.RegisterAluForm()
    if form.validate_on_submit():
        flash('Registro Exitoso','success')
        models.Alumno.create_alumno(
            idAlumno = form.idAlumno.data,
            Nombres = form.Nombres.data,
            Apellido1 = form.Apellido1.data,
            Apellido2 = form.Apellido2.data,
            Telefono = form.Telefono.data,
            NSS = form.NSS.data,
            Genero = Genero,
            idGrupo = idGrupo
        )
        return redirect(url_for('index'))
    return render_template('register_alu.html', form=form)


@app.route('/register_prac', methods=('GET','POST'))
def register_prac():
    form = forms.RegisterPracForm()
    if form.validate_on_submit():
        flash('Registro Exitoso','success')
        models.Practica.idPractica.auto_increment=True
        models.Practica.create_prac(
            NumeroPractica = form.NumeroPractica.data,
            TipoDePractica = form.TipoDePractica.data,
            FechaRealizacion = form.FechaRealizacion.data,
            PresupuestoAsignado = form.PresupuestoAsignado.data,
            Objetivo = form.Objetivo.data,
            CompetenciaADesarrollar = form.CompetenciaADesarrollar.data,
            EstrategiaDesarrollo = form.EstrategiaDesarrollo.data,
            idGrupo = form.idGrupo.data,
            idEmpresa = form.idEmpresa.data
        )
        return redirect(url_for('index'))
    return render_template('register_prac.html', form=form)


@app.route('/register_doc', methods=('GET', 'POST'))
def register_doc():
    form = forms.RegisterDocForm()
    if form.validate_on_submit():
        flash('Fuiste Registrado!!!', 'success')
        models.Docente.create_doc(
            idDocente = form.idDocente.data,
            Nombres = form.Nombres.data,
            Apellido1 =  form.Apellido1.data,
            Apellido2 = form.Apellido2.data,
            Genero = form.Genero.data,
            password = form.password.data
        )
        return redirect(url_for('index'))
    return render_template('register.html', form=form)


@app.route('/login', methods=('GET', 'POST'))
def login():
    form = forms.LoginForm()
    if form.validate_on_submit():
        try:
            user = models.Docente.get(models.Docente.idDocente == form.idDocente.data)
        except models.DoesNotExist:
            flash('Tu nombre de usuario o contraseña no existe', 'error')
        else:
            if check_password_hash(user.password, form.password.data):
                login_user(user)
                flash('Has iniciado sesión', 'success')
                return redirect(url_for('index'))
    return render_template('login.html', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash('Tu sesion a concluido', 'success')
    return redirect(url_for('index'))


@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


@app.route('/')
def index():
    return  'HEY'

if __name__ == '__main__':
    models.initialize()
    try:
        models.Administrador.create_admin(
            idAdministrador='123',
            Nombres='Israel',
            Apellido1='Cianca',
            Apellido2 = 'Vazquez',
            password='admin',
        )
    except ValueError:
        pass
    app.run(debug=DEBUG, host=HOST, port=PORT)
